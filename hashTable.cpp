#include<iostream>
#include<string>
#include<fstream>
#include<sstream>
#include <math.h>       /* modf */

#include "hashTable.hpp"

using namespace std;

HashTable::HashTable() {
  size = 10;
  table = new HashNode *[10];
  for (int i = 0; i < size; i++) table[i] = 0;
}

HashTable::HashTable(int s) {
    size = s;
    table = new HashNode *[s];
    for (int i = 0; i < size; i++) table[i] = 0;
  }

HashTable:: ~HashTable() {
  for (int i = 0; i < size; i++) {
    HashNode* tmp = table[i];
    while (tmp != 0) {
      HashNode* curr = tmp;
      tmp = tmp->next;
      delete curr;
    }
  }
  delete[] table;
}


void HashTable::insert(string key, string value) {
  
  int index = hashCode(key, size);
  
  HashNode* tmp = new HashNode(key, value);
  
  if (table[index] == 0) {
    table[index] = tmp;
  }
  else {
    HashNode* curr = table[index];
    HashNode* prev = 0;
    
    while (curr != 0) {
      if (curr->key == key) {
	if (curr->value == value) 
	  cout << " Error: Entry exists in the hashtable!\n";
	else
	  cout << " Error: Duplicate key!\n";
	return;
      }
      
      prev = curr;
      curr = curr->next;
    }
    
    prev->next = tmp;
    return;
  }
}

HashNode* HashTable::search(string key) {
  int index = hashCode(key, size);
  
  HashNode* tmp = table[index];
  
  while (tmp != 0) {
    if (tmp->key == key) return tmp;
    tmp = tmp->next;
  }
  
  return 0;
}

void HashTable::remove(string key) {
  int index = hashCode(key, size);
  
  HashNode* tmp = table[index];
  HashNode* prev = 0;
  
  while (tmp != 0) {
    if (tmp->key == key) break;
    else {
      prev = tmp;
      tmp = tmp->next;
    }
  }
  
  if (tmp == 0) return;
  else {
    if (prev == 0) {
      table[index] = tmp->next;
      delete tmp;
      tmp = 0; 
    }
    else {
      prev->next = tmp->next;
      delete tmp;
      tmp = 0; 
    }
  }
  return;
}


int HashTable::hashCode(string k, int table_size) {
  int hash = 0;
  
  for (int i = 0; i< k.length();  i++) 
    hash = hash + k[i];
  
  return hash % table_size;
}

/******************************************************************************
 *
 *  1. Given a key k (k is a string), generate the sum of the ASCII values
 *      for the characters in k.
 *  2. Multiply k by a constant A, where 0 < A < 1.
 *  3. Store the fractional part of kA.
 *  4. Multiply fractional part of kA by a constant, m, and take the floor of the result. 
 *
 *********************************************************************************/

int HashTable::hashCode2(string k, int table_size) {
    int hash = 0;
    double A = 13.0/32.0;
    double hA;
    int m = 1024;
    
    for (int i = 0; i< k.length();  i++) 
      hash = hash + k[i];
    
    hA = hash * A;
    
    double fractpart, intpart;
    //get the fractional part of hash 
    fractpart = modf (hA , &intpart);
    
    fractpart = fractpart*m;
    hash = floor(fractpart);
    return hash % table_size;
  }


void HashTable::printHashTable() {
  for (int i = 0; i < size; i++) {
    if (table[i] != 0) {
      cout << "[" << i << "] ";
      HashNode* curr = table[i];
      while (curr != 0) {
	cout << "-> Key: " << curr->key << " (#" << hashCode(curr->key, size)<< ") Value: " << curr->value; 	  
	curr = curr->next;
      }
      cout << endl;
    }
    else {
      cout << "[" << i << "] -> Empty" << endl;
    }
  }
}
