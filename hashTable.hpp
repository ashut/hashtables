#include<iostream>
using namespace std;

struct HashNode {
  string key;
  string value;
  HashNode* next;
  
  HashNode() {key = ""; value = ""; next = 0;}
  HashNode(string k, string v) {key = k; value = v; next = 0;}
  ~HashNode() {}
};

class HashTable {
  HashNode** table;
  int size;

public:

  HashTable();  
  HashTable(int s);

  ~HashTable();

  void insert(string key, string value);
  HashNode* search(string key);
  void remove(string key);
  
  int hashCode(string k, int table_size);
  int hashCode2(string k, int table_size);
  
  void printHashTable();
};
